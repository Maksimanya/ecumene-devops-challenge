Prerequisites
In order to start CI/CD you need to set Environment Variables at Settings > CI/CD > Variables. 

1. AWS_ACCESS_KEY_ID           - variable -    AWS credentials to access AWS console API.
2. AWS_SECRET_ACCESS_KEY       - variable -    AWS credentials to access AWS console API.
3. TF_VAR_private_key_val      -   file   -    Create Key-pair with name "AWS-Cisco" in AWS console, then copy contents of pem file into this variable. 
4. TF_VAR_state_backend_token  - variable -    SignUp to app.terraform.io. Then create organization "ecumene-challenge" with workspace "aws". Set execution mode to "local". Create token at UserSettings > Tokens > aws. Copy token into this variable.

Lastly, to execute Pipeline you need to assign tags to commits you want to deploy.

