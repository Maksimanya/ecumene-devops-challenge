# official Python runtime, used as the parent image
FROM python:3.10.0a2-alpine3.12

ARG VERSION=1.0
ARG user=challenge
ARG group=challenge
ARG uid=1000
ARG gid=1000

RUN addgroup -g ${gid} ${group}
RUN adduser -h /home/${user} -u ${uid} -G ${group} -D ${user}

LABEL Description="This is a python app image, which provides the production-ready executables" Vendor="Ecumene Challenger" Version="${VERSION}"

# set the working directory in the container to /app
WORKDIR /home/${user}/app

# add the current directory to the container as /app
ADD . /home/${user}/app

# execute pip install -r
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# expose port 8000 for the Bottle app to run on
EXPOSE 8000

# execute the Bottle app
CMD ["python", "server.py"]