variable "ec2_region" {
  default = "eu-central-1"
}

variable "ec2_image" {
  default = "ami-0bd39c806c2335b95"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_keypair" {
  default = "AWS-Cisco"
}

variable "ec2_tags" {
  default = "Challenge-Terraform-2"
}

variable "ec2_count" {
  default = "1"
}

variable "private_key_val" {
  type = string
}

variable "doc_img_tag" {
  type = string
}