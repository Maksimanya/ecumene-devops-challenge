resource "aws_instance" "OneServer" {
  ami           = var.ec2_image
  instance_type = var.ec2_instance_type
  key_name      = var.ec2_keypair
  count         = var.ec2_count
  security_groups = [data.aws_security_group.allow-tls.name]
  iam_instance_profile = aws_iam_instance_profile.iam_profile.name

  tags = {
    Name = var.ec2_tags
  }
  provisioner "file" {
    source      = "../docker-compose.yml"
    destination = "/tmp/docker-compose.yml"
    connection {
        type     = "ssh"
        user     = "ec2-user"
        private_key = file("${path.module}/pem_file.pem")
        timeout = "2m"
        agent = false
        host = self.public_dns
    }
  }
  provisioner "file" {
    source      = "../mysite.template"
    destination = "/tmp/mysite.template"
    connection {
        type     = "ssh"
        user     = "ec2-user"
        private_key = file("${path.module}/pem_file.pem")
        timeout = "2m"
        agent = false
        host = self.public_dns
    }
  }
  provisioner "file" {
    source      = "../run_nginx.sh"
    destination = "/tmp/run_nginx.sh"
    connection {
        type     = "ssh"
        user     = "ec2-user"
        private_key = file("${path.module}/pem_file.pem")
        timeout = "2m"
        agent = false
        host = self.public_dns
    }
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y amazon-cloudwatch-agent",
      "sudo amazon-linux-extras install -y docker",
      "sudo service docker start",
      "sudo usermod -a -G docker ec2-user",
      "sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose",
      "sudo chmod +x /tmp/run_nginx.sh",
      "/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a stop",
      "exit"
    ]
    connection {
        type     = "ssh"
        user     = "ec2-user"
        private_key = file("${path.module}/pem_file.pem")
        timeout = "2m"
        agent = false
        host = self.public_dns
    }
  }
  provisioner "file" {
    source      = "./amazon-cloudwatch-agent.json"
    destination = "/tmp/amazon-cloudwatch-agent.json"
    connection {
        type     = "ssh"
        user     = "ec2-user"
        private_key = file("${path.module}/pem_file.pem")
        timeout = "2m"
        agent = false
        host = self.public_dns
    }
  }
  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/amazon-cloudwatch-agent.json /opt/aws/amazon-cloudwatch-agent/etc/",
      "sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json -s",
      "docker network create int_conn",
      "cd /tmp && HOST=${self.public_dns} TAG=${var.doc_img_tag} docker-compose up -d"
    ]
    connection {
        type     = "ssh"
        user     = "ec2-user"
        private_key = file("${path.module}/pem_file.pem")
        timeout = "2m"
        agent = false
        host = self.public_dns
    }
  }
}

resource "null_resource" "update" {
  triggers = {
    always_run = timestamp()
  }
  connection {
    type     = "ssh"
    user     = "ec2-user"
    private_key = file("${path.module}/pem_file.pem")
    timeout = "2m"
    agent = false
    host = element(aws_instance.OneServer.*.public_ip, 0)
  }

  provisioner "remote-exec" {
    inline = [
      "cd /tmp && HOST=$(curl http://169.254.169.254/latest/meta-data/public-hostname) TAG=${var.doc_img_tag} docker-compose up --force-recreate --build -d"
    ]
  }
}

output "instance_ip_addr" {
  value       = aws_instance.OneServer.*.private_ip
  description = "The private IP address of the main server instance."
}

output "instance_ips" {
  value = aws_instance.OneServer.*.public_ip
}