#!/usr/bin/env bash
export DOLLAR='$'
envsubst < /tmp/mysite.template > /etc/nginx/conf.d/default.conf
nginx -g "daemon off;"